import math


class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):  # tutaj dodajemy produkt do zamówienia
        self.products.append(product)

    # @property # Funkcja, która zmienia atrybyty klasy we właściwości. Bez property zwraca None gdy nie ma zamówienia.
    def get_total_price(self):  # Funkcja określająca całkowity koszt zamówienia klienta
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price
        # return sum(product.get_price()
        #     for product in self.products)

    def get_total_quantity_of_products(self):  # funkcja która przechowuje ilości produktów
        # quantity = 0
        # for product in self.products:
        #     quantity += product.quantity
        #     return quantity
        return sum(product.quantity for product in self.products)

    def purchase(self):  # Tutaj potwierdzamy zamówienie/zakup
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1.0):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):  # Ta metoda zwraca koszt produktu względem zamówionej ilości
        return self.unit_price * self.quantity

    def change_price(self, new_price):  # Zmiana ceny na produkcie na nową wartość
        self.unit_price = new_price

    def change_quantity(self, new_quantity):  # Zmiana ilości na produkcie na nową wartość
        self.quantity = new_quantity

    def get_new_price(self):  # Koszt za zakup produktu przy uwzględnieniu zamówionej ilości
        return self.unit_price * product.quantity


if __name__ == '__main__':
    product = Product("Shoes", 100.0)
    print(f"It is a name of the product: {product.name}")
    print(f"It is a price for the product: {product.unit_price}")
    print(f"It is a quantity of the product: {product.quantity}")
    print(f"This is the cost of the product relative to the quantity ordered: {product.get_price():.2f}")

    product.change_price(30)
    print(f"New price of the product is: {product.unit_price}")

    product.change_quantity(3.0)
    print(f"New quantity of the product is: {product.quantity}")
    print(f"This is the new cost of the product relative to the new quantity ordered: {product.get_new_price():.2f}")

    order = Order('adrian@example.com')
    print(f"This is the email of the customer who places the order: {order.customer_email}")
    print(f"It is a total price for zero orders: {order.get_total_price()}")
    print(f"It is a total quantity of products: {order.get_total_quantity_of_products()}")
    print(f"This is how much goods our customer has ordered so far: {order.purchase()}")
